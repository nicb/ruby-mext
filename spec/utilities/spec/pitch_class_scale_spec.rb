require 'spec_helper'
require File.expand_path(File.join(['..'] * 2, 'pitch_class_scale'), __FILE__)

RSpec.describe 'Mext::Spec::Utilities.pitch_class_scale' do

  before :example do
    @default_increment = 1
    @bot = Mext::Spec::Utilities::DEFAULT_BOTTOM_PITCH
    @top = Mext::Spec::Utilities::DEFAULT_TOP_PITCH
    @other_args = [[-3.0, 3.0, 2], [-4.0, -3.05, 1], [4.0, 4.05, 1], [@bot, @top, 0.25]]
    @eps = 10e-8
  end

  it 'can be called without argument and does the proper thing' do
    res = []
    Mext::Spec::Utilities.pitch_class_scale { |n| res << n }
    expect(res.empty?).not_to be true
    last = res[0]
    res[1..-1].each do
      |cur|
      expect(cur).to eq(last.addsemi(@default_increment))
      last = cur
    end
  end

  it 'can be called with arguments and does the proper thing' do
    @other_args.each do
      |s, e, inc|
      res = []
      Mext::Spec::Utilities.pitch_class_scale(s, e, inc) { |n| res << n }
      expect(res.empty?).not_to be true
      last = res[0]
      res[1..-1].each do
        |cur|
        should_be = last.addsemi(inc)
        expect(cur).to(be_within(@eps).of(should_be), ("pitch: %.4f != %.4f, inc: %.4f" % [cur, should_be, inc]))
        last = cur
      end
    end
  end

end
