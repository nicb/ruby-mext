module Mext
  module Spec
    module Utilities

      DEFAULT_BOTTOM_PITCH = -17.0
      DEFAULT_TOP_PITCH    =  17.0

      class << self

        #
        # FIXME: this scale function works for ascending scales only.
        # it requires the condition in the while to be fixed in order in order
        # to work for descending ones
        #
        def pitch_class_scale(bot = DEFAULT_BOTTOM_PITCH, top = DEFAULT_TOP_PITCH, semi_inc = 1)
          cur = bot
          while(cur < top)
            yield(cur)
            cur = cur.addsemi(semi_inc)
          end
        end

      end

    end
  end
end
