require 'spec_helper'

RSpec.describe Mext::RandomGenerator do

  before :example do
    @eps = 1e-10
    @fixed_seed = 104513627723317647589994116793407121203.to_s
    @first_10_numbers = [0.7593446076380375, 0.6846482358169717, 0.22071344413418625, 0.15755893142238409, 0.13487197099171544, 0.6469694762287067, 0.9700646865429291, 0.9697991973809669, 0.1459192928749803, 0.6805606892758506]
    ENV.delete(Mext::RandomGenerator::MEXT_SEED_TAG)
  end

  it 'cannot be created' do
    expect{ Mext::RandomGenerator.new }.to raise_error(NoMethodError)
  end

  it 'has at least one singleton instance' do
    expect(Mext::PRNG).not_to be nil
  end

  it 'has at least one singleton instance that works' do
    1.upto(10) { |n| expect(Mext::PRNG.r).to be_between(0.0, 1.0) }
  end

  it 'has an instance which can tell its own seed' do
    expect((seed = Mext::PRNG.seed)).not_to be nil
  end

  it 'can be set with a seed and will behave deterministically' do
    #
    # seed and run should be predictable
    #
    ENV[Mext::RandomGenerator::MEXT_SEED_TAG] = @fixed_seed
    rg = Mext::RandomGenerator.send(:new)
    @first_10_numbers.each do
      |n|
      expect((val = rg.r)).to(be_within(@eps).of(n), "draw: #{val} != #{n}")
    end
    #
    # unseed and run should be unpredictable
    #
    ENV.delete(Mext::RandomGenerator::MEXT_SEED_TAG)
    expect(ENV[Mext::RandomGenerator::MEXT_SEED_TAG]).to be nil
    rg = Mext::RandomGenerator.send(:new)
    @first_10_numbers.each do
      |n|
      expect((val = rg.r)).not_to(be_within(@eps).of(n), "draw: #{val} != #{n}")
    end
    #
    # seed and run should be predictable again
    #
    ENV[Mext::RandomGenerator::MEXT_SEED_TAG] = @fixed_seed
    rg = Mext::RandomGenerator.send(:new)
    @first_10_numbers.each do
      |n|
      expect((val = rg.r)).to(be_within(@eps).of(n), "draw: #{val} != #{n}")
    end
  end

  it 'can obtain deterministic behaviour also with the default object' do
    expect(Mext::PRNG.seed).not_to(eq(@fixed_seed))
    Mext::PRNG.seed = (@fixed_seed.to_i)
    data = []
    1.upto(100) { data << Mext::PRNG.r }
    Mext::PRNG.seed = (1234)
    data.each { |n| expect(Mext::PRNG.r).not_to(eq(n)) }
    Mext::PRNG.seed = (@fixed_seed.to_i)
    data.each { |n| expect(n).to(be_within(@eps).of(Mext::PRNG.r)) }
  end

end
