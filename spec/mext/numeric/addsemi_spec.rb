require "spec_helper"
require File.expand_path(File.join('..', 'numeric_tester'), __FILE__)

describe 'Numeric::addsemi' do

  before :example do
    @eps = 1e-6
    @dataset =
    [
      NumericTester.new(8.00, 96.0),
      NumericTester.new(8.09, 105.0),
      NumericTester.new(8.09025, 105.025),
      NumericTester.new(0.01,    1.0),
      NumericTester.new(0.00,    0.0),
      NumericTester.new(-0.01,  -1.0),
      NumericTester.new(-1.00, -12.0),
      NumericTester.new(-1.04, -16.0),
      NumericTester.new(-1.01, -13.0),
      NumericTester.new(-1.09, -21.0),
      NumericTester.new(-1.10, -22.0),
    ]
    @bot_semi = -60.0
    @top_semi =  60.0
  end

  it 'has a addsemi method that works' do
    @dataset.each do
      |el|
      @bot_semi.step(@top_semi, 0.25).each do
        |semi|
        expect((should_be = Mext::Music::PitchClass.create_from_semitones(el.from.pchtosemi + semi))).not_to be nil
        expect(el.from.addsemi(semi)).to(be_within(@eps).of(should_be.to_f), "#{el.from}.addsemi(#{semi}) (#{el.from.addsemi(semi)}) != #{should_be.to_f}")
      end
    end
  end

end
