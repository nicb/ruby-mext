require "spec_helper"

describe 'Numeric::mround' do

  before :example do
    @values = 1.upto(10).map { (rand * 20) - 10 }
  end

  it 'has a mround method that works for float values' do
    @values.each do
      |v|
      0.upto(12).each do
        |dig|
        pow = 10**(dig)
        should_be = (((v * pow).round)/pow.to_f)
        expect(v.mround(dig)).to(eq(should_be), "#{v}.mround(#{dig}) = #{v.mround(dig)} != #{should_be}")
      end
    end
  end

  it 'does work with no argument (i.e. with a default)' do
    @values.each do
      |v|
      expect(v.mround).to(eq(v.round), "#{v}.mround = #{v.mround} != #{v.round}")
    end
  end

end
