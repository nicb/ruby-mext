require 'spec_helper'

describe Mext::Utilities do

  class MextUtilitiesTester
    include Mext::Utilities
  end

  before :example do
    @instance_methods = []
    @class_methods = [:from_yaml]
  end

  it 'allows classes including it to be created' do
    expect(MextUtilitiesTester.new).not_to be nil
  end

  it 'has all the needed instance methods' do
    expect((b = MextUtilitiesTester.new)).not_to be nil
    @instance_methods.each do
       |m|
       expect(b.respond_to?(m)).to(be(true), "#respond_to?(:#{m}) is false")
    end
  end

  it 'has all the needed class methods' do
    @class_methods.each do
       |m|
       expect(MextUtilitiesTester.respond_to?(m)).to(be(true), ".respond_to?(:#{m}) is false")
    end
  end

end
