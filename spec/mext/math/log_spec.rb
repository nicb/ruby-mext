require "spec_helper"
require 'yaml'
require 'log_dataset'

describe Math::Log do

  before :example do
    @eps = 1e-6
    @arg_sets = Mext::Spec::Fixtures::Math::LOG_DATASETS
    @yaml_data = YAML.load(File.open(File.join(Mext::Spec::Fixtures::PATH, 'log.yml'), 'r'))
  end

  it 'can be created' do
    expect(Math::Log.new(*@arg_sets[0][0..4])).not_to be nil
  end

  it 'passes all the expon dataset tests' do
    @arg_sets.each do
       |args|
       (ys, ye, xs, xe, base, xmid, ymid) = args
       expect((fun = Math::Log.new(ys, ye, xs, xe, base))).not_to be nil
       expect((result = fun.y(xs))).to(be_within(@eps).of(ys), "#{result} != #{ys} for #{xs}")
       expect((result = fun.y(xe))).to(be_within(@eps).of(ye), "#{result} != #{ye} for #{xe}")
       expect((result = fun.y(xmid))).to(be_within(@eps).of(ymid), "#{result} != #{ymid} for #{xmid} (base = #{fun.base})")
    end
  end

  it 'can be created from an appropriate yaml file' do
    expect((fun = Math::Log.from_yaml(@yaml_data))).not_to be nil
    args = @arg_sets.first
    (ys, ye, xs, xe, base, xmid, ymid) = args
    expect(base).not_to be nil # necessary to shut up warnings
    expect((result = fun.y(xs))).to(be_within(@eps).of(ys), "#{result} != #{ys} for #{xs}")
    expect((result = fun.y(xe))).to(be_within(@eps).of(ye), "#{result} != #{ye} for #{xe}")
    expect((result = fun.y(xmid))).to(be_within(@eps).of(ymid), "#{result} != #{ymid} for #{xmid} (base = #{fun.base})")
  end

end
