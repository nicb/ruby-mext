require "spec_helper"

describe Math::Function do

  before :example do
    @hash_arg = {} # no args for the abstract class
    @instance_methods = [:label, :y, :xy]
  end

  it 'cannot be created (pure abstract class)' do
    expect { Math::Function.new }.to raise_error(Mext::PureAbstractMethodCalled)
  end
  

  #
  # fake concrete function
  #
  class FakeFunction < Math::Function

    private

      def setup
      end
  end

  it 'responds to a number of instance methods' do
    expect((f = FakeFunction.new)).not_to be nil
    @instance_methods.each do
      |m|
      expect(f.respond_to?(m)).to(be(true), "did not respond to the :#{m} method")
    end
  end

  it 'can be created through the :from_yaml method' do
    expect(FakeFunction.from_yaml(@hash_arg)).not_to be nil
  end
  
end
