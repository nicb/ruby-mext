require "spec_helper"

describe 'Mext::Music::Meter' do

  before :example do
    @dataset = [[4,4],[8,8], [2,2], [3,4], [5,16], ]
    @string_dataset = ['4/4', '8/8', '2 /4', '3 / 4', '5/ 16',]
    @wrong_strings = ['wrong', 'wrong/wrong',]
    @logic_dataset =
    [
      { args: [[4, 4], [8, 8]], eq: true, cmp: 0 },
      { args: [[4, 4], [3, 4]], eq: false, cmp: 1 },
      { args: [[5, 16], [8, 8]], eq: false, cmp: -1 },
    ]
    @arithmetic_dataset =
    [
      { args: [[4, 4], [8, 8]], plus: '2/1'.to_meter, minus: '0/1'.to_meter, mul: '1/1'.to_meter, div: '1/1'.to_meter, pow: '1/1'.to_meter  },
      { args: [[4, 4], [3, 4]], plus: '7/4'.to_meter, minus: '1/4'.to_meter, mul: '3/4'.to_meter, div: '4/3'.to_meter, pow: '1/1'.to_meter  },
      { args: [[3, 4], [4, 4]], plus: '7/4'.to_meter, minus: '-1/4'.to_meter, mul: '3/4'.to_meter, div: '3/4'.to_meter, pow: '3/4'.to_meter  },
      { args: [[5, 16], [7, 12]], plus: '43/48'.to_meter, minus: '-13/48'.to_meter, mul: '35/192'.to_meter, div: '15/28'.to_meter, pow: '142813404121235.0/281474976710656.0'.to_meter },
    ]
    @numeric_dataset = [ 0, 0.0, 5, 3/4, 1/2 ]
  end

  it 'has a Rational-like method to create Meters' do
    @dataset.each do
      |n, d|
      expect((m = Meter(n, d))).not_to be nil
      expect(m.numerator).to eq(n)
      expect(m.divisor).to eq(d)
    end
  end

  it 'extends String with a to_meter method that works' do
    @string_dataset.each do
      |s|
      expect((m = s.to_meter)).not_to be nil
      (ns, ds) = s.split(/\s*\/\s*/)
      n = ns.to_f
      d = ds.to_f
      expect(m.numerator).to eq(n)
      expect(m.divisor).to eq(d)
    end
  end

  it 'fails graciously when passed a wrong string' do
    @wrong_strings.each do
      |s|
      expect((m = s.to_meter)).not_to be nil
      expect(m.numerator).to eq(0.0)
      expect(m.divisor).to eq(0.0)
    end
  end

  it 'has functional logic operators' do
    @logic_dataset.each do
      |hash|
      a = hash[:args][0]
      b = hash[:args][1]
      expect((m0 = Meter(a[0], a[1]))).not_to be nil
      expect((m1 = Meter(b[0], b[1]))).not_to be nil
      expect(m0 == m1).to(eq(hash[:eq]), "#{m0} == #{m1} does not correspond to #{hash[:eq]}")
      expect(m0 <=> m1).to(eq(hash[:cmp]), "#{m0} <=> #{m1} does not correspond to #{hash[:cmp]}")
    end
  end

  it 'has functional arithmetic operators' do
    @arithmetic_dataset.each do
      |hash|
      a = hash[:args][0]
      b = hash[:args][1]
      expect((m0 = Meter(a[0], a[1]))).not_to be nil
      expect((m1 = Meter(b[0], b[1]))).not_to be nil
      expect(m0 + m1).to(eq(hash[:plus]), "#{m0} + #{m1} does not correspond to #{hash[:plus]}")
      expect(m0 - m1).to(eq(hash[:minus]), "#{m0} - #{m1} does not correspond to #{hash[:-]}")
      expect(m0 * m1).to(eq(hash[:mul]), "#{m0} * #{m1} does not correspond to #{hash[:mul]}")
      expect(m0 / m1).to(eq(hash[:div]), "#{m0} / #{m1} does not correspond to #{hash[:/]}")
      expect(m0 ** m1).to(eq(hash[:pow]), "#{m0} ** #{m1} does not correspond to #{hash[:pow]}")
    end
  end

  it 'can be converted from any kind of number' do
    @numeric_dataset.each { |n| expect(n.to_meter).not_to be nil }
  end

end
