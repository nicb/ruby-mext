require "spec_helper"

describe 'Mext::Music::NoteName' do

  before :example do
    @correct_note_names = %w(A B C D E F G)
    @note_numbers = [9, 11, 0, 2, 4, 5, 7]
    @weird_note_names = # just to test the lilypond conversion
    {
      'A###' => 'aisisis8',
      'Afff' => 'aeseses8',
      'B#'   => 'bis8',
      'Cf'   => 'ces8',
      'DFF'  => 'deses8',
      'A'    => 'a8',
      'f#'   => 'fis8',
      'Ffff' => 'feseses8',
    }
    @not_notes = ' '.ord.upto('A'.ord-1).map { |n| n.chr } 
    @not_notes.concat('H'.ord.upto('a'.ord-1).map { |n| n.chr })
    @not_notes.concat('h'.ord.upto((2**7)-2).map { |n| n.chr })
    @eps = 1e-8
  end

  it 'can be created with a name and an index' do
    @correct_note_names.each_index { |i| expect(Mext::Music::NoteName.new(@correct_note_names[i], i)).not_to be nil }
  end

  it 'performs the to_lily conversion correctly' do
    @weird_note_names.each do
      |k, should_be|
      # while we test the lilypond conversion the index does not really
      # matter, now does it?
      nn = Mext::Music::NoteName.new(k, 0) # index not used
      expect((res = nn.to_lily)).to(eq(should_be), ":to_lily(#{k}) => #{res} != #{should_be}")
    end
  end

  it 'raises the appropriate errors on non-note conversion to lilypond notation' do
    @not_notes.each do
      |n|
      nn = Mext::Music::NoteName.new(n, 0) # index not used
      expect { nn.to_lily }.to raise_error(Mext::Music::NotALilypondName, "\"#{nn.name}\" does not contain a note")
    end
  end

  it 'performs the :to_pitch_class conversion correctly' do
    0.upto(15) do  # these are the octaves
      |oct|
      @correct_note_names.each_index do
        |idx|
        cnn = @correct_note_names[idx]
        cidx = @note_numbers[idx]
        nn = Mext::Music::NoteName.new(cnn, cidx)
        should_be = oct + (cidx / 100.0)
        expect((res = nn.to_pitch_class(oct))).to(eq(should_be), "#{nn.inspect}.to_pitch_class(#{oct}) = #{res} != #{should_be}")
      end
    end
  end

  it 'performs the :to_frequency conversion correctly' do
    0.upto(15) do  # these are the octaves
      |oct|
      @correct_note_names.each_index do
        |idx|
        cnn = @correct_note_names[idx]
        cidx = @note_numbers[idx]
        nn = Mext::Music::NoteName.new(cnn, cidx)
        should_be = (oct + (cidx / 100.0)).pchcps
        expect((res = nn.to_frequency(oct))).to(be_within(@eps).of(should_be), "#{nn.inspect}.to_frequency(#{oct}) = #{res} is far from #{should_be}")
      end
    end
  end

end
