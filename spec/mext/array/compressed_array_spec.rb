require "spec_helper"

describe Mext::CompressedArray do

  before :example do
    @eps = 1e-6
    #
    # test with different type of arrays
    #
    @dataset =
    [
      { arg: [ "a", "b", "c", "d" ], should_be: [ "a", "b", "c", "d" ] },
      { arg: [ ["a", 4], "b", [23, 2], ], should_be: [ 'a', 'a', 'a', 'a', 'b', 23, 23 ], },
      { arg: [ ["a", 4, 3], "b", [23, 2], ], should_be: [ ['a', 4, 3], 'b', 23, 23 ], },
    ]
    @yaml_hash = { 'args' => @dataset[2][:arg] }
    @yaml_hash_should_be = @dataset[2][:should_be]
  end

  it 'can be created with no arguments' do
    expect((a = Mext::CompressedArray.new)).not_to be nil
    expect(a.kind_of?(Array)).to be true
    expect(a.size).to eq(0)
  end

  it 'can be created with another array' do
    @dataset.each do
      |hash|
      test = hash[:arg]
      result = hash[:should_be]
      expect((a = Mext::CompressedArray.new(test))).not_to be nil
      expect(a.kind_of?(Array)).to be true
      expect(a.size).to eq(result.size)
    end
  end

  it 'expands the argument array properly' do
    @dataset.each do
      |hash|
      test = hash[:arg]
      result = hash[:should_be]
      expect((a = Mext::CompressedArray.new(test))).not_to be nil
      expect(a).to eq(result)
    end
  end

  it 'can be created with a yaml hash' do
    expect((a = Mext::CompressedArray.from_yaml(@yaml_hash))).not_to be nil
    expect(a).to eq(@yaml_hash_should_be)
  end

end
