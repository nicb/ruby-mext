require "spec_helper"

describe "Array::fill_float" do

  class FillFloatTester
    attr_reader :sv, :ev, :stp

    def initialize(sv, ev, stp)
      @sv = sv; @ev = ev; @stp = stp
    end

    def size_should_be
      ((self.ev - self.sv) / self.stp).floor
    end

    def last_should_be
      self.ev - self.stp
    end

  end

  class FunkyFillFloatTester < FillFloatTester
    attr_reader :size_should_be

    def initialize(sv, ev, stp, ssb)
      super(sv, ev, stp)
      @size_should_be = ssb
    end

  end

  before :example do
    @eps = 1e-6
    @dataset =
    [
      FillFloatTester.new(-1, 1,  0.001),
      FillFloatTester.new( 1,-1, -0.001),
      FillFloatTester.new( -5,  5,  1),
    ]
    @dataset_with_default_steps =
    [
      FillFloatTester.new(-1, 1,  1.0),
      FillFloatTester.new( 1,-1,  1.0),
      FillFloatTester.new( -5,  5,  1.0),
    ]
    @dataset_with_full_defaults =
    [
      FillFloatTester.new(0, 1,  1.0),
      FillFloatTester.new(0,-1,  1.0),
      FillFloatTester.new(0, 5,  1.0),
    ]
    @exception_dataset =
    [
      FillFloatTester.new( -5, -5,  0),
    ]
    @zero_dataset =
    [
      FillFloatTester.new(  0, -5,  1),
    ]
    @funky_dataset =
    [
      FunkyFillFloatTester.new(  0,  1,  3, 0),
      FunkyFillFloatTester.new(  0,  8,  5, 1),
    ]
  end

  it 'has an Array::fill_float method' do
    expect(Array.respond_to?(:fill_float)).to be true
  end

  it 'has an Array::fill_float method that works' do
    @dataset.each do
      |fft|
      expect((a = Array.fill_float(fft.ev, fft.sv, fft.stp))).not_to be nil
      expect(a.size).to(be_within(@eps).of(fft.size_should_be), "a.size = #{a.size} != #{fft.size_should_be}")
      expect(a.first).to(be_within(@eps).of(fft.sv), "a.first = #{a.first} != #{fft.sv}")
      expect(a.last).to(be_within(@eps).of(fft.last_should_be), "a.last = #{a.last} != #{fft.last_should_be}")
      expect(a[1]).to(be_within(@eps).of(fft.sv+fft.stp), "a[1] = #{a[1]} != #{fft.sv+fft.stp}")
      expect(a[-2]).to(be_within(@eps).of(fft.last_should_be-fft.stp), "a[-2] = #{a[-2]} != #{fft.last_should_be-fft.stp}")
      idx = 0
      fft.sv.step(fft.last_should_be, fft.stp).each do
        |fv|
        expect(a[idx]).to(be_within(@eps).of(fv), "a[#{idx}] = #{a[idx]} != #{fv}")
        idx += 1
      end
    end
  end

  it 'has an Array::fill_float method that works also with defaults' do
    #
    # default step 1.0
    #
    @dataset_with_default_steps.each do
      |fft|
      expect((a = Array.fill_float(fft.ev, fft.sv))).not_to be nil
      sz = fft.ev - fft.sv
      sz = sz > 0 ? sz : 0
      unless sz == 0
        expect(a.size).to(be_within(@eps).of(sz), "a.size = #{a.size} != #{fft.size_should_be}")
        expect(a.first).to(be_within(@eps).of(fft.sv), "a.first = #{a.first} != #{fft.sv}")
        expect(a.last).to(be_within(@eps).of(fft.last_should_be), "a.last = #{a.last} != #{fft.last_should_be}")
        expect(a[1]).to(be_within(@eps).of(fft.sv+1), "a[1] = #{a[1]} != #{fft.sv+1}")
        expect(a[-2]).to(be_within(@eps).of(fft.ev-(fft.stp*2)), "a[-2] = #{a[-2]} != #{fft.ev-(fft.stp*2)}")
        idx = 0
        fft.sv.step(fft.ev-fft.stp, 1).each do
          |fv|
          expect(a[idx]).to(eq(fv), "a[#{idx}] = #{a[idx]} != #{fv}")
          idx += 1
        end
      end
    end
    #
    # default step 1.0, start value 0.0
    #
    @dataset_with_full_defaults.each do
      |fft|
      expect((a = Array.fill_float(fft.ev))).not_to be nil
      sz = fft.ev
      sz = sz < 0 ? 0 : sz
      unless sz == 0
        expect(a.size).to(be_within(@eps).of(sz), "a.size = #{a.size} != #{sz}")
        expect(a.first).to(be_within(@eps).of(0.0), "a.first = #{a.first} != 0.0")
        expect(a.last).to(be_within(@eps).of(fft.last_should_be), "a.last = #{a.last} != #{fft.last_should_be}")
        unless sz < 2
          expect(a[1]).to(be_within(@eps).of(0.0+1), "a[1] = #{a[1]} != #{0.0+1}")
          expect(a[-2]).to(be_within(@eps).of(fft.last_should_be-1), "a[-2] = #{a[-2]} != #{fft.last_should_be-1}")
        end
        idx = 0
        0.0.step(fft.last_should_be, 1).each do
          |fv|
          expect(a[idx]).to(eq(fv), "a[#{idx}] = #{a[idx]} != #{fv}")
          idx += 1
        end
      end
    end
  end

  it 'cannot be created with non-conforming arguments' do
    @exception_dataset.each do
      |fft|
      expect { Array.fill_float(fft.sv, fft.ev, fft.stp) }.to raise_error(ArgumentError)
    end
  end

  it 'creates zero-sized arrays with stupid arguments' do
    @zero_dataset.each do
      |fft|
      expect((a = Array.fill_float(fft.ev, fft.sv, fft.stp))).not_to be nil
      expect(a.size).to(eq(0))
    end
  end

  it 'creates funky arrays with funky arguments' do
    @funky_dataset.each do
      |fft|
      expect((a = Array.fill_float(fft.ev, fft.sv, fft.stp))).not_to be nil
      expect(a.size).to(eq(fft.size_should_be))
    end
  end

end
