require "spec_helper"

Array::DOT_OPERATIONS.each do
  |method, op|

  describe "Array::#{method}" do

    before :example do
      @eps = 1e-6
      @dataset_1 =
      [
        [0, 1, 2, 3, 4, 5],
        [ 0 ],
        [],
        [ 1 ] * 1e5,
      ]
      @dataset_2 =
      [
        [6, 7, 8, 9, 10, 11],
        [ 23e15 ],
        [],
        [ 2 ] * 1e5,
      ]
    end

    it "has a :#{method.to_s} that works" do
      expect(@dataset_1[0].respond_to?(method)).to be true
      @dataset_1.each_index do
        |idx|
        res = @dataset_1[idx].send(method, @dataset_2[idx])
        res.each_index { |jdx| expect(res[jdx]).to(be_within(@eps).of(@dataset_1[idx][jdx].send(op, @dataset_2[idx][jdx])), "#{res[jdx]} != #{@dataset_1[idx][jdx]} #{op} #{@dataset_2[idx][jdx]}") }
      end
    end

    it 'raises an error when the sizes are different' do
      expect { @dataset_1[0].send(method, @dataset_2[1]) }.to(raise_error(Array::SizeMismatch), "#{method}")
    end

  end

end
