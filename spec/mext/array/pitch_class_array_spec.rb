require "spec_helper"

describe Mext::PitchClassArray do

  before :example do
    @eps = 1e-6
    @pitches =
    [
      8.07, 8.07, 8.11, 8.11, 8.01, 9.03, nil, 8.05, 8.06, nil,
      8.07, 7.09, 8.06, 9.03, 8.07, 8.11, 8.01, 8.05, 9.00,
      8.06, 8.08, 8.09, 8.03, 8.07, 8.11, 9.01, 9.05, 8.06,
      8.08, 8.09, 8.03, 8.07, 8.11, 8.01, 9.06, nil,  8.02,
      8.08, 8.06, 8.09, 9.03, 7.07, 7.11, 9.01, 9.05, 8.10
    ]
    @yaml_hash = { 'args' => @pitches }
  end

  it 'can be created with no arguments' do
    expect((a = Mext::PitchClassArray.new)).not_to be nil
    expect(a.kind_of?(Array)).to be true
    expect(a.size).to eq(0)
  end

  it 'can be created with another array' do
    expect((a = Mext::PitchClassArray.new(@pitches))).not_to be nil
    expect(a.kind_of?(Array)).to be true
    expect(a.size).to eq(@pitches.size)
  end

  it 'has only PitchClass or nil members' do
    expect((a = Mext::PitchClassArray.new(@pitches))).not_to be nil
    a.each { |pch| expect(pch.kind_of?(Mext::Music::PitchClass)).to(be(true), "#{pch.class} returns false") if pch }
  end

  it 'can be created with a yaml hash' do
    expect((a = Mext::PitchClassArray.from_yaml(@yaml_hash))).not_to be nil
    a.each { |pch| expect(pch.kind_of?(Mext::Music::PitchClass)).to(be(true), "#{pch.class} returns false") if pch }
  end

  it 'has an :intervals method that works' do
    expect((a = Mext::PitchClassArray.new(@pitches))).not_to be nil
    expect((intvs = a.intervals)).not_to be nil
    a[0..-2].each_index do
      |idx|
      if a[idx] && a[idx+1]
        expect((a[idx].add_semitones(intvs[idx])).to_f).to(eq(a[idx+1].to_f), "a[#{idx}](#{a[idx].to_f})+intvs[#{idx}](#{intvs[idx]}) != a[#{idx+1}](#{a[idx+1].to_f})")
      end
    end
  end

end
