require "spec_helper"

describe Mext::EndlessArray do

  class EndlessArrayTester
    attr_reader :sv, :ev, :stp

    def initialize(sv, ev, stp)
      @sv = sv; @ev = ev; @stp = stp
    end

    def size_should_be
      ((self.ev - self.sv) / self.stp).floor
    end

    def to_a
       Array.fill_float(self.ev, self.sv, self.stp)
    end

  end

  before :example do
    @eps = 1e-6
    @dataset =
    [
      EndlessArrayTester.new(-1,  1, 0.01),
      EndlessArrayTester.new(23, 24, 0.1),
    ]
    @rounds = 10
    @restart_values_perc = [0.1, 0.25, 0.5, 0.75, 0.99]
    @steps = Array.fill_float(6, 2)
    @yaml_hashes = [
      [{ 'args' => [0, "1/3.0", "-23.0+(2/3.0)"] }, [0, 0.333333333, -22.33333333]],
      [{ 'args' => ["4*(2+(Math::sqrt(5.0)/2.0))", "-16.015.abs", -4e16] },
       [12.47213595499958, 16.015, -4.0e+16]],
    ]
  end

  it 'can be created with no arguments' do
    expect((a = Mext::EndlessArray.new)).not_to be nil
    expect(a.kind_of?(Array)).to be true
    expect(a.size).to eq(0)
  end

  it 'can be created with another array' do
    @dataset.each do
      |eat|
      expect((a = Mext::EndlessArray.new(eat.to_a))).not_to be nil
      expect(a.kind_of?(Array)).to be true
      expect(a.size).to eq(eat.size_should_be)
    end
  end

  it 'can be created with a yaml hash' do
    @yaml_hashes.each do
      |yh|
      args = yh[0]
      should_be = yh[1]
      expect((ea = Mext::EndlessArray.from_yaml(args))).not_to be nil
      should_be.each_index do
        |idx|
        expect(ea[idx]).to(be_within(@eps).of(should_be[idx]), "#{ea[idx]} != #{should_be[idx]}")
      end
    end
  end

  it 'has a :next method that works' do
    @dataset.each do
      |eat|
      eata = eat.to_a
      testds = []
      1.upto(@rounds) { testds.concat(eata) }
      expect((a = Mext::EndlessArray.new(eata))).not_to be nil
      expect(a.size).to eq(eat.size_should_be)
      testds.each { |el| expect((a_el = a.next)).to(eq(el), "a.next = #{a_el} != #{el}") }
    end
  end

  it 'has a :previous method that works' do
    @dataset.each do
      |eat|
      eata = eat.to_a
      testds = []
      1.upto(@rounds) { testds.concat(eata) }
      testds << eata.first # needs the first value
      testds.reverse!
      expect((a = Mext::EndlessArray.new(eata))).not_to be nil
      expect(a.size).to eq(eat.size_should_be)
      testds.each do
        |el|
        expect((a_el = a.previous)).to(eq(el), "a.previous (a[#{a.cur}]) = #{a_el} != #{el}")
      end
    end
  end

  it 'has a :next method that works with different restart values' do
    @dataset.each do
      |eat|
      expect((a = Mext::EndlessArray.new(eat.to_a))).not_to be nil
      @restart_values_perc.each do
        |rvp|
        rvpidx = (rvp*a.size).floor
        testds = [ ]
        1.upto(@rounds) { testds.concat(eat.to_a[rvpidx..-1]) }
        expect(a.restart_value = rvpidx).not_to be nil
        expect(a.size).to eq(eat.size_should_be)
        idx = 0
        testds.each do
          |el|
          expect((a_el = a.next)).to(eq(el), "a.next (a[#{a.cur}]) = #{a_el} != #{el} (testds[#{idx}]) with rvp = #{rvp}")
          idx += 1
        end
      end
    end
  end

  it 'has a :previous method that works with different restart values' do
    @dataset.each do
      |eat|
      @restart_values_perc.each do
        |rvp|
        eata = eat.to_a
        expect((a = Mext::EndlessArray.new(eata))).not_to be nil
        rvpidx = (rvp*a.size).floor
        testds = [ ]
        1.upto(@rounds) { testds.concat(eata[rvpidx..-1]) }
        testds.reverse!
        expect(a.restart_value = rvpidx).not_to be nil
        expect(a.size).to eq(eat.size_should_be)
        testds.unshift(a.current) # add the current value at the top
        idx = 0
        testds.each do
          |el|
          expect((a_el = a.previous)).to(eq(el), "a.previous (a[#{a.cur}]) = #{a_el} != #{el} (testds[#{idx}]) with rvp = #{rvp}")
          idx += 1
        end
      end
    end
  end

  it 'has a :next method that works with different steps' do
    @dataset.each do
      |eat|
      @steps.each do
        |stp|
        testds = [ ]
        idx = stp
        eata = eat.to_a
        expect((a = Mext::EndlessArray.new(eata))).not_to be nil
        1.upto(@rounds) do
          0.step(eata.size - 1, stp).each do
            |indx|
            testds << eata[idx]
            idx += stp
            idx %= eat.size_should_be
          end
        end
        testds.unshift(a.current) # add the current value at the top
        expect(a.size).to eq(eat.size_should_be)
        idx = 0
        testds.each do
          |el|
          expect((a_el = a.next(stp))).to(eq(el), "a.next (a[#{a.cur}]) = #{a_el} != #{el} (testds[#{idx}]) with step = #{stp}")
          idx += 1
        end
      end
    end
  end

  it 'has a :previous method that works with different steps' do
    @dataset.each do
      |eat|
      @steps.each do
        |stp|
        testds = [ ]
        idx = 0
        eata = eat.to_a
        expect((a = Mext::EndlessArray.new(eata))).not_to be nil
        reata = eata.reverse
        reata.unshift(eata[0])
        1.upto(@rounds) do
          0.step(reata.size - stp, stp).each do
            |indx|
            testds << reata[idx]
            idx += stp
            idx %= eat.size_should_be
          end
        end
        expect(a.size).to eq(eat.size_should_be)
        idx = 0
        testds.each do
          |el|
          expect((a_el = a.previous(stp))).to(eq(el), "a.previous (a[#{a.cur}]) = #{a_el} != #{el} (testds[#{idx}]) with step = #{stp}")
          idx += 1
        end
      end
    end
  end

  it 'has a :reset method that works' do
    @dataset.each do
      |eat|
      expect((a = Mext::EndlessArray.new(eat.to_a))).not_to be nil
      @restart_values_perc.each do
        |rvp|
        rvpidx = (rvp*a.size).floor
        expect(a.restart_value=(rvpidx)).not_to be nil
        should_be = a.current
        1.upto(4) { a.next } # move it
        expect((rv = a.reset)).to(eq(should_be), "a.reset = #{rv} != #{should_be}")
      end
    end
  end

  it 'has a :peek_next method that works' do
    @dataset.each do
      |eat|
      eata = eat.to_a
      testds = []
      1.upto(@rounds) { testds.concat(eata) }
      expect((a = Mext::EndlessArray.new(eata))).not_to be nil
      expect(a.size).to eq(eat.size_should_be)
      testds[1..-1].each do
        |el|
        expect((a_el = a.peek_next)).to(eq(el), "a.peek_next = #{a_el} != #{el}")
        a.next
      end
    end
  end

  it 'has a :peek_next method that works with different steps' do
    @dataset.each do
      |eat|
      @steps.each do
        |stp|
        testds = [ ]
        idx = stp
        eata = eat.to_a
        expect((a = Mext::EndlessArray.new(eata))).not_to be nil
        1.upto(@rounds) do
          0.step(eata.size - 1, stp).each do
            |indx|
            testds << eata[idx]
            idx += stp
            idx %= eat.size_should_be
          end
        end
        testds.unshift(a.current) # add the current value at the top
        expect(a.size).to eq(eat.size_should_be)
        idx = 0
        testds[1..-1].each do
          |el|
          expect((a_el = a.peek_next(stp))).to(eq(el), "a.peek_next (a[#{a.cur+stp}]) = #{a_el} != #{el} (testds[#{idx}]) with step = #{stp}")
          idx += 1
          a.next(stp)
        end
      end
    end
  end

  it 'has a :peek_next method that works with different restart values' do
    @dataset.each do
      |eat|
      expect((a = Mext::EndlessArray.new(eat.to_a))).not_to be nil
      @restart_values_perc.each do
        |rvp|
        a.reset
        rvpidx = (rvp*a.size).floor
        testds = [ ]
        1.upto(@rounds) { testds.concat(eat.to_a[rvpidx..-1]) }
        expect(a.restart_value = rvpidx).not_to be nil
        expect(a.size).to eq(eat.size_should_be)
        idx = 0
        testds[1..-rvpidx].each do
          |el|
#byebug if idx == 0
          expect((a_el = a.peek_next)).to(eq(el), "a.peek_next (a[#{a.cur+1}]) = #{a_el} != #{el} (testds[#{idx}]) with rvp = #{rvp}")
          idx += 1
          a.next
        end
      end
    end
  end

  it 'has a :next method that works even at the very beginning' do
    @dataset.each do
      |eat|
      expect((a = Mext::EndlessArray.new(eat.to_a))).not_to be nil
      expect(a.next).to eq(eat.to_a[0])
    end
  end

end
