require "spec_helper"

Array::SCALAR_OPERATIONS.each do
  |method|

  describe "scalar Array::#{method}" do

    before :example do
      @eps = 1e-6
      @dataset_1 =
      [
        [0, 1, 2, 3, 4, 5],
        [ 0 ],
        [],
        [ 1 ] * 1e5,
      ]
      @scalar_value = 23.23
    end

    it "has a scalar :#{method.to_s} that works" do
      @dataset_1.each do
        |ds|
        expect(ds.respond_to?(method)).to be true
        res = ds.send(method, @scalar_value)
        res.each_index { |idx| expect(res[idx]).to(be_within(@eps).of(ds[idx].send(method, @scalar_value)), "#{res[idx]} != #{ds[idx]} #{method} #{@scalar_value}") }
      end
    end

    it "handles things properly when no scalar is used with method :#{method}" do
      @dataset_1.each do
        |ds|
        res = ds.send(method, ds)
        expect(res.size).to(eq(ds.size.send(method, ds.size)), "a #{method} b")
      end
    end

  end

end

Array::SCALAR_NEW_OPERATIONS.each do
  |method|

  describe "scalar Array::#{method}" do

    before :example do
      @eps = 1e-6
      @dataset_1 =
      [
        [0, 1, 2, 3, 4, 5],
        [ 0 ],
        [],
        [ 1 ] * 1e5,
      ]
      @scalar_value = 23.23
    end

    it "has a scalar :#{method.to_s} that works" do
      @dataset_1.each do
        |ds|
        expect(ds.respond_to?(method)).to be true
        res = ds.send(method, @scalar_value)
        res.each_index { |idx| expect(res[idx]).to(be_within(@eps).of(ds[idx].send(method, @scalar_value)), "#{res[idx]} != #{ds[idx]} #{method} #{@scalar_value}") }
      end
    end

    it "handles things properly when no scalar is used with method :#{method}" do
      @dataset_1.each do
        |ds|
        expect { ds.send(method, ds) }.to(raise_error(TypeError), "#{method}") unless ds.empty?
      end
    end

  end

end
