require "spec_helper"
require 'yaml'

describe Mext::Sound::Info do

  before :example do
    @eps = 1e-6
    @instance_methods = [:filename, :sound_dir, :sample_rate, :channels, :num_frames, :dur]
    @sound_dir = File.expand_path(File.join(['..'] * 3, 'fixtures', 'sounds'), __FILE__)
    @dataset = YAML.load(File.open(File.join(@sound_dir, 'sounds.yml'), 'r'))
  end

  it 'can be created with two arguments' do
    @dataset.each do
      |fname, values|
      expect(Mext::Sound::Info.new(fname, @sound_dir)).not_to be nil
    end
  end

  it 'does respond to all methods' do
    @dataset.each do
      |fname, values|
      expect((si = Mext::Sound::Info.new(fname, @sound_dir))).not_to be nil
      @instance_methods.each do
        |im|
        expect(si.respond_to?(im)).to(be(true), "instance method :#{im} does not exist")
      end
    end
  end

  it 'has methods that actually do work' do
    @dataset.each do
      |fname, values|
      expect((si = Mext::Sound::Info.new(fname, @sound_dir))).not_to be nil
      values.each do
        |im, value|
        expect(si.send(im)).to(be_within(@eps).of(value), "Mext::Sound::Info##{im} = #{si.send(im)} != #{value}")
      end
    end
  end

end
