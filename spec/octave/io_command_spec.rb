require "spec_helper"

describe 'Octave::Io.command' do

  before :example do
    @xs = [-15, -10, 0, 10, 15]
    @ys = [0.08, -12, -20, -31, 0.9]
    @command_string = "a=polyfit(#{@xs}, #{@ys}, #{@xs.size}); a'"
    @expected_results = [7.6961e-05, 0.000848533, -0.017193598, -0.099853333, -0.000248355, -20.0]
    @eps = 1e-6
  end

  it 'does its job' do
    expect((res = Octave::Io.command(@command_string))).not_to be nil
    expect(res.empty?).not_to be true
    res.each_index { |idx| expect(res[idx]).to be_within(@eps).of(@expected_results[idx]) }
  end
  
end
