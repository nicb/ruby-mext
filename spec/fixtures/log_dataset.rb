module Mext
  module Spec
    module Fixtures
      module Math

        LOG_DATASETS =
        [
          [ 10, 1, 10, 100, 10, 55.0, 9.306976221629778 ],
          [ 10, 0, 10, 100, 2, 55.0, 9.30689821833927 ],
          [ 0, 10, 10, 100, 2, 55.0, 9.30689821833927 ],
          [ 1, 10, 0, 1, 3, 0.5, 9.306976221629778 ],
        ]

      end
    end
  end
end
