module Mext
  module Octave

    PATH = File.expand_path(File.join('..', 'octave'), __FILE__)

  end
end

%w(
  io
  polyfit
).each { |f| require File.join(Mext::Octave::PATH, f) }
