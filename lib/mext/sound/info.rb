require 'wavefile'

module Mext
  module Sound

    #
    # +Mext::Sound::Info+
    #
    # +Mext::Sound::Info+ is a class that holds all the information
    # about a given sound sample.
    #
    # PLEASE NOTE: current implementation only supports wav files.
    # You are welcome to contribute a more encompassing one.
    #
    class Info
      attr_reader :filename, :sound_dir
      attr_reader :sample_rate, :channels, :num_frames

      def initialize(f, dir)
        @filename = f
        @sound_dir = dir
        setup
      end

      #
      # +dur+
      #
      # returns the total duration in seconds, calculating it with
      # sample precision
      #
      def dur
        self.num_frames.to_f / self.sample_rate.to_f
      end

    private

      def setup
        path = File.join(self.sound_dir, self.filename)
        WaveFile::Reader.new(path) do
          |reader|
          @sample_rate = reader.format.sample_rate
          @num_frames  = reader.total_sample_frames
          @total_dur   = reader.total_duration    # FIXME: kept but not used for now
          @channels    = reader.format.channels
        end
      end

    end

  end
end
