module Mext

  MUSIC_PATH = File.join(Mext::PATH, 'music')

end

%w(
  pitch_class
  meter
  note_name
  note_names
).each { |f| require File.join(Mext::MUSIC_PATH, f) }
