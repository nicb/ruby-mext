class Numeric

  #
  # +mround(digits = 0)+: round to the nearest +digit+ digits
  #
  # +mround+ will return a rounded number to the nearest digit indicated
  #          in its argument
  #
  #:nodoc:

  def mround(digit = 0)
    mul = (10**(digit)).to_f
    res = (self * mul).round
    res / mul
  end

end
