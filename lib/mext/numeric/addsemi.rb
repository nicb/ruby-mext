class Numeric

  #:doc:
  #
  # +addsemi+: add semitones to a pitch class float number
  #
  # interprets its receiver as a pitch class and returns the
  # corresponding addition of semitones
  #
  #:nodoc:

  def addsemi(semi)
    full_semi = self.pchtosemi

    (full_semi + semi).semitopch
  end

end
