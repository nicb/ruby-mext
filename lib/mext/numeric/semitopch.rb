class Numeric

  #:doc:
  #
  # +semitopch+: semitone to pitch class converter
  #
  # interprets its receiver as a semitone quantity (starting from 0 at
  # pitch class 0.00) and returns its corresponing pitch class
  #
  #:nodoc:

  def semitopch

    oct  = (self / CNPO).mround(8).to_i    # rounding corrects modulo aberrations
    semi = (self - (oct * CNPO)).mround(8) # rounding corrects modulo aberrations
    semi %= CNPO
    semi = (self >= 0.0) ? semi : -((CNPO - semi) % CNPO)

    oct + (semi / PCC)

  end

end
