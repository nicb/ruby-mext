require 'cmath'

module Math

  class Log < Function

    attr_reader :y_start, :y_end, :base
    attr_reader :a, :b, :c

    #
    # +DEFAULT_BASE+: default logarithmic curvature factor for +Log()+
    #
    DEFAULT_BASE = Math::E

    #
    # +Math::Log.new(ystart, yend, xstart, xend, base = DEFAULT_BASE)+:
    #
    # exponential curve `y = e^(a*x + b) + c` where:
    #
    # `c = yend + base`
    #
    # Arguments are:
    #
    # +ystart+, +yend+: start/end y values required
    # +xstart+, +xend+: start/end x values
    # +base+: the curvature factor
    #
    #:nodoc:
    def initialize(ys, ye, xs, xe, base = DEFAULT_BASE)
      @y_start = ys
      @y_end   = ye
      @x_start = xs
      @x_end   = xe
      @base     = base
      setup
    end

    #:doc:
    #
    # +y(x)+:
    #
    # Returns a real value (forcing any complex result to its modulus) for any
    # given x
    #
    #:nodoc:
    def y(x)
      (CMath::log(self.a*x + self.b) + self.c).abs   # we want a real number result, no complex please
    end

    def label
      "base: #{self.base}"
    end

    class << self

      #
      # +from_yaml(yaml_hash)+:
      #
      # creates a Math::Log class from a yaml file which must have the
      # relevant fields:
      #
      # +x_start+
      # +x_end+
      # +y_start+
      # +y_end+
      # +base+
      #
      def from_yaml(yh)
        args = [yh['y_start'], yh['y_end'], yh['x_start'], yh['x_end'], yh['base']]
        new(*args)
      end

    end

  private

    def setup
       @c = self.y_end + self.base

       log_sv = self.y_start - self.c        # start value of the exponential
       log_ev = self.y_end - self.c          # end value of the exponential
       x_length = self.x_end - self.x_start

       @a = (CMath::exp(log_ev) - CMath::exp(log_sv)) / x_length;
       @b= CMath::exp(log_sv) - (self.a * x_start);
    end

  end

end

