module Math

  class Polynomial < Function

    #
    # +:values+
    #
    # is an array of pairs (+x+, +y+)
    #
    attr_reader :values, :factors

    #
    # +Math::Polynomial.new(values = [])+
    #
    # stepwise function
    #
    # Arguments are:
    #
    # +values+:         an array of (+x+, +y+) pairs
    #
    #:nodoc:
    def initialize(vs = [])
      setup(vs)
    end

    #:doc:
    #
    # +y(x)+:
    #
    # Returns a value for any given x
    #
    #:nodoc:
    def y(x)
      calculate(x)
    end

    def label
      'polynomial function'
    end

    class << self

      #
      # +from_yaml(yaml_hash)+:
      #
      # creates a Math::Polynomial class from a yaml file which must have the
      # relevant fields:
      #
      # +values+: an array of duples [x, y]
      #
      def from_yaml(yh)
        new(yh['values'])
      end

    end

  private

    def calculate(x)
      result = 0
      idx = self.factors.size-1
      fidx = 0
      idx.downto(0) do
        |n|
        result += (self.factors[fidx]*(x**(n)))
        fidx += 1
      end
      result
    end

    #
    # +setup+
    #
    # calls +octave+'s +polyfit+ function to load the factors of the
    # polynomial
    #
    #:nodoc:
    def setup(vs)
      @values = []
      sorted = vs.sort { |a, b| a[0] <=> b[0] }
      @values.concat(sorted)
      xs = self.values.map { |v| v[0] }
      ys = self.values.map { |v| v[1] }
      porder = vs.size
      @factors = ::Octave::Io::polyfit(xs, ys, porder)
      #
      # keep in mind that vs might be empty
      #
      @x_start = self.values.first ? self.values.first.first : self.values.first
      @x_end   = self.values.last ? self.values.last.first : self.values.last
    end

  end

end
