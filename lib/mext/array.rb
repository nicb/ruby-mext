module Mext

  ARRAY_PATH = File.join(Mext::PATH, 'array')

end

%w(
  vectorize
  choose
  dotop
  scalarop
  fill
  endless_array
  compressed_array
  pitch_class_array
).each { |f| require File.join(Mext::ARRAY_PATH, f) }
