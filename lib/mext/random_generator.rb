module Mext

  #
  # +Mext::RandomGenerator+
  #
  # singleton seedable version of the default Random Generator
  #
  class RandomGenerator

    private_class_method :new

    MEXT_SEED_TAG = 'MEXT_SEED'

    def initialize
      @mext_seed = ENV[MEXT_SEED_TAG] ? ENV[MEXT_SEED_TAG] : (ENV[MEXT_SEED_TAG] = (rand()*10e20).to_i.to_s)
      self.seed = @mext_seed
    end

    def seed
      @mext_seed.to_i
    end

    def seed=(val)
      Math.send(:srand, val.to_i)
    end

    def r
      Math.send(:rand)
    end

  end

  PRNG = RandomGenerator.send(:new)

end
