module Mext
  module Numeric

    PATH = File.join(Mext::PATH, 'numeric')
    VECTORIZABLE_METHODS = %w(
      mround
      cround
      mtof
      ftom
      ampdb
      dbamp
      pchtooctsemi
      pchtosemi
      semitopch
      pchtom
      mtopch
      cpspch
      pchcps
      gold
      rrand
      mmtot
      addsemi
    )
    NON_VECTORIZABLE_METHODS = %w(
      constants
      pitch_fork
    )
    ADDED_METHODS = NON_VECTORIZABLE_METHODS + VECTORIZABLE_METHODS

  end
end

Mext::Numeric::ADDED_METHODS.each { |f| require File.join(Mext::Numeric::PATH, f) }
