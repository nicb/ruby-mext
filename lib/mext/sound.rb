module Mext
  module Sound

    PATH = File.expand_path(File.join('..', 'sound'), __FILE__)

  end
end

%w(
  info
).each { |f| require File.join(Mext::Sound::PATH, f) }
