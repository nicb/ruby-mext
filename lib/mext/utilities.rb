module Mext

  #
  # +Mext::Utilities+
  #
  # add common methods to several +Mext+ library classes.
  #
  # It allows a common interface to several utilities
  #
  module Utilities

    def self.included(base)
      base.extend ClassMethods
    end

    module ClassMethods

      #
      # +from_yaml(yaml_hash)+
      #
      # allows to create from a yaml hash
      #
      def from_yaml(yh)
        args = yh.values
        new(*args)
      end

    end

  end

end
