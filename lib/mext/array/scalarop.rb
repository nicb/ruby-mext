class Array

  #
  # modifying the behaviour of scalar multiplication for +Array+s
  # has the unintended side-effect of killing any +File.join()+ path which
  # contains a +['..'] * n+ idiomatic figure.
  #
  # Better not tamper with it and use another name
  #

  SCALAR_OPERATIONS = [ :+, :-, ]
  SCALAR_NEW_OPERATIONS = [ :smul, :/, :%, :**, ]

  SCALAR_OPERATIONS.each do
    |m|
    orig_meth = "orig_#{m.to_s}".to_sym
    alias_method orig_meth, m
    define_method(m) { |sc| scalarop(m, sc) }
  end

  SCALAR_NEW_OPERATIONS.each do
    |m|
    define_method(m) { |sc| scalarop_new(m, sc) }
  end

protected

  def scalarop_new(op, other)
    self.map { |el| el.send(op, other) }
  end

  def scalarop(op, other)
    orig_meth = "orig_#{op.to_s}".to_sym
    return self.send(orig_meth, other) unless other.kind_of?(Numeric)
    scalarop_new(op, other)
  end

end

#
# we add an +:smul+ method to scalars in order to seamlessly interoperate
# with the array version of +:smul+
#
class Numeric

  def smul(other)
    self * other
  end

end
