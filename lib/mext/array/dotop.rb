class Array

  class SizeMismatch < StandardError; end

  DOT_OPERATIONS =
  {
    :dotplus => :+,
    :dotminus => :-,
    :dotmul  => :*,
    :dotdiv  => :/,
    :dotmod  => :%,
    :dotpow  => :**,
    :dotaddsemi => :addsemi,
  }

  DOT_OPERATIONS.each { |op, m| define_method(op) { |other| dotop(m, other) } }

protected

  def dotop(op, other)
    raise SizeMismatch unless self.size == other.size
    res = []
    self.each_index { |idx| res << (self[idx].send(op, other[idx])) }
    res
  end

end
