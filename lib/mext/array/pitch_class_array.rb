module Mext

  #
  # +Mext::PitchClassArray+
  #
  # a kind of +Mext::EndlessArray+ which 
  # has a secondary +Mext::EndlessArray+ of the interval
  # relations among 
  # or the previous value in an endless fashion (i.e. +mod'ding+
  # the current index)
  #
  class PitchClassArray < EndlessArray

    attr_reader :intervals

    #
    # +Mext::PitchClassArray.new(me = [], rv = 0)+
    #
    # create an endless array which is supposed to
    # be filled up with pitch classes
    #
    def initialize(me = [], rv = 0)
      super
      condition_pitches!
      @intervals = create_interval_array
    end

  private

    #
    # +condition_pitches!+ transform all numbers in pitch classes 
    # for further processing. We check that n is not nil to manage
    # rests and so on.
    #
    def condition_pitches!
      self.each_index do
        |idx| 
        if self[idx]
          tmp = Mext::Music::PitchClass.new(self[idx].to_f)
          self[idx] = tmp
        end
      end
    end

    #
    # the interval array is an array of semitones (not pitch classes)
    #
    def create_interval_array
      res = EndlessArray.new
      last_pch = self.first
      unless self.empty?
        self[1..-1].each do
          |pch|
          intv = last_pch && pch ? (pch - last_pch).to_semitones : nil
          res << intv
          last_pch = pch
        end
      end
      res
    end

  end

end
