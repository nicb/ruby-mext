class Array

  #
  # +choose+: choose a random element in the array
  #
  # returns a random element from the array
  #
  #:nodoc:

  def choose
    idx = (Mext::PRNG.r*(self.size-1)).round
    self[idx]
  end

end
