class Array

  class << self

    #
    # +fill_float(end_value, start_value = 0.0, step = 1.0)+
    #
    # fill a blank newly created array with +Float+ numbers which will range
    # from +start_value+ to +end_value+ in steps of +step+
    #
    MINIMUM_STEP = 1e-32

    def fill_float(end_value, start_value = 0.0, step = 1.0)
      raise ArgumentError.new("step too small") unless step.abs > MINIMUM_STEP
      res = new
      ev = end_value - step
      start_value.step(ev, step).each { |f| res << f }
      res
    end

  end

end
