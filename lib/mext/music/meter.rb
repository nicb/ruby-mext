module Mext
  module Music

    #
    # +Meter+:
    #
    # in music we cannot use the +Rational+ class
    # because the latter will make all due conversions
    # simplifying meters (like: 4/4 => 1/1), which is not what we want
    #
    class Meter
  
      attr_accessor :numerator, :divisor

      alias_method :denominator, :divisor
  
      def initialize(n, d)
        self.numerator = n.to_f
        self.divisor   = d.to_f
      end

      def to_r
        raise ZeroDivisionError, "#{self.numerator}/#{self.divisor} provokes a zero-division error" unless self.divisor != 0
        Rational(self.numerator, self.divisor)
      end

      def to_s
        "#{self.numerator}/#{self.divisor}"
      end
      #
      # we use the logic of +Rational+ to perform logic on +Meter+
      #
      [:==, :<, :<=, :>, :>=, :<=>, :===].each { |m| define_method(m) { |other| common_logic(m, other) } }
      #
      # we use the arithmetic of +Rational+ to perform arithmetic on +Meter+
      #
      [:+, :-, :*, :/, :**, :^, ].each { |m| define_method(m) { |other| common_arithmetic(m, other) } }


    private

      def common_logic(method, other)
        raise ArgumentError, "#{other} is neither a #{self.class} nor a Rational (#{other.class})" unless other.kind_of?(Meter) || other.kind_of?(Rational)
        self.to_r.send(method, other.to_r)
      end

      def common_arithmetic(method, other)
        self.to_r.send(method, other.to_r).to_meter
      end
  
    end

  end
end

#
# +Meter(n, d)+ is defined to actually mimick a +Rational()+
#
def Meter(n, d)
  Mext::Music::Meter.new(n,d)
end

#
# we extend the +String+ class to carry a +to_meter+ which
# will convert a string into a meter
#
class String

  def to_meter
    div = num = 1.0
    (nums, divs) = self.split(/\s*\/\s*/, 2)
    num = nums.to_f
    div = divs.to_f
    Meter(num, div)
  end

end

#
# we extend the +Rational+ class to carry a +to_meter+ which
# will convert a  into a Meter class
#
class Rational

  def to_meter
    Meter(self.numerator, self.denominator)
  end

end

#
# we extend the +Numeric+ class to carry a +to_meter+ which
# will convert a  into a Meter class
#
class Numeric

  def to_meter
    r = self.to_r
    Meter(r.numerator, r.denominator)
  end

end
