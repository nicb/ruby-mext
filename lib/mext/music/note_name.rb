module Mext
  module Music
    #
    # +Mext::Music::NoteName+
    #
    # produces all the conversions from a named note to +pitch frequency+,
    # +pitch class+, +LilyPond notation+, etc.
    #
    # In these conversions we assume that 8 is the central octave.
    #
    class NotALilypondName < StandardError; end
    class NoteName

      DEFAULT_NOTE_NAME_OFFSET = 9 # central 'A' 440 is 8.09, isn't it?
      LILY_NOTE_NAMES = %w( a b c d e f )

      attr_reader :name, :index

      def initialize(name, idx)
        @name = name
        @index = idx
      end

      def to_lily(octave = 8, other = '') # 8 is the central octave
        nn = self.name.sub(/\A\s*(.).*$/, '\1').downcase
        raise NotALilypondName, "\"#{self.name}\" does not contain a note" unless LILY_NOTE_NAMES.include?(nn)
        sharps = (self.name.match?(/\A\s*\w#/) && self.name.sub(/\A\s*\w(#+).*$/, '\1').gsub(/#/, 'is')) || nil
        flats = (self.name.match?(/\A\s*\w[Ff]/) && self.name.sub(/\A\s*\w([Ff]+).*$/, '\1').gsub(/[Ff]/, 'es')) || nil
        accs = sharps ? sharps : (flats ? flats : '')
        "#{nn}#{accs}#{octave}#{other}"
      end

      def to_pitch_class(octave = 8) # 8 is the central octave
        octave + (self.index/100.0)
      end

      def to_frequency(octave = 8)
        self.to_pitch_class(octave).pchcps
      end

    end
  end
end
