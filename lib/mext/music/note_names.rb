module Mext
  module Music

    DEFAULT_ANGLO_NAMES = %w(A B C D E F G)
    DEFAULT_ITA_NAMES   = %w(Do Re Mi Fa Sol La Si)

  end
end
