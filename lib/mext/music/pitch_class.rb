module Mext
  module Music

    #
    # +Mext::Music::PitchClass+
    #
    # this is a representation of pitches in the usual +PitchClass+
    # representation of +csound+ and other software
    #
    # Internally, the representation is in semitones. In order to be
    # consistent with the +csound+ representation, a +PitchClass+ of
    # +8.00+ represents +central C4+ (+ca. 261.3 Hz+, depending on tuning),
    # while a +PitchClass+ of +0.00+ is 96 semitones below (8 * 12).
    #
    # The internal representation allows for floating point values, negative
    # values etc. However, since the +PitchClass+ is a geometric
    # representation of pitches, it will never produce a negative
    # frequency value (for semitones that go to -Inf., it'll produce
    # infinitely small positive values.
    #
    class PitchClass
  
      #
      # +Mext::Music::PitchClass.new(float_value, semi = nil)+:
      #
      # pitch class object, where argument is:
      #
      # +float_value+: a pitch class in float notation (i.e. 8.00 for middle C, etc.)
      #                +or+ an octave value when given in octave, semi pair (see below)
      # +semi+: (optional) when this argument is not nil, then it is assumed
      #         that the first argument is the octave, and this one is the
      #         semitone value
      #
      #:nodoc:
      def initialize(fv, semi = nil)
        @semi = semi ? setup_with_two_arguments(fv, semi) : setup_with_one_argument(fv)
      end

      #:doc:
      #
      # +octave+
      #
      # returns the octave part of the pitch class
      #
      def octave
        res = @semi / ::Numeric::CNPO
        res >= 0.0 ? res.floor : res.ceil
      end
  
      #:doc:
      #
      # +semi+
      #
      # returns the semitone part of the pitch class
      #
      def semi
        @semi >= 0.0 ? (@semi % 12.0) : (@semi % -12.0)
      end

      #:doc:
      #
      # +inner_representation+
      #
      # returns the inner representation in semitones (this is a public method
      # since it is used in several comparison methods)
      #
      #:no_doc:
      def inner_representation
        @semi
      end
  
      #:doc:
      #
      # +to_f+
      #
      # returns the +pitch class+ in float notation
      #
      #:nodoc:
      def to_f
        self.inner_representation.semitopch
      end
  
      #:doc:
      #
      # +to_freq+
      #
      # returns the +pitch class+ in frequency (Hz)
      #
      #:nodoc:
      def to_freq
        self.inner_representation.semitopch.pchcps
      end
  
      #:doc:
      #
      # all logical operators
      # 
      #
      #:nodoc:
      [:>, :>=, :<, :<=, :<=>, :!=, :=== ].each do
        |op|
        define_method(op) { |other| self.inner_representation.send(op, other.inner_representation) }
      end

      def ==(other)
        self.inner_representation.cround == other.inner_representation.cround
      end

      #:doc:
      #
      # +\+(other)+ (operator plus)
      #
      # sums two pitch classes
      #
      #:nodoc:
      def +(other)
        self.class.create_from_semitones((self.inner_representation + other.inner_representation).cround)
      end

      #:doc:
      #
      # +-(other)+ (operator minus)
      #
      # subtracts two pitch classes
      # 
      #
      #:nodoc:
      def -(other)
        self.class.create_from_semitones((self.inner_representation - other.inner_representation).cround)
      end

      #:doc:
      #
      # +to_semitones+
      #
      # returns the +PitchClass+ to a number of semitones
      #
      alias_method :to_semitones, :inner_representation
  
      #:doc:
      #
      # +interval(other)+
      #
      # computes the interval among two pitch classes (in
      # number of semitones and fractions thereof)
      #
      #:nodoc:
      def interval(other)
        (other - self).inner_representation
      end

      #:doc:
      #
      # +transpose(semitones)+
      #
      # returns a +PitchClass+ transposed by +semitones+ semitones
      #
      #:nodoc:
      def transpose(semi)
        self.class.create_from_semitones((self.inner_representation + semi).cround)
      end

      #:doc:
      #
      # +transpose!(semitones)+
      #
      # transposes the receiver by +semitones+ semitones
      #
      #:nodoc:
      def transpose!(semi)
        @semi += semi
        self
      end

      #:doc:
      #
      # +interval_proportion(prop, other)+:
      #
      # returns the interval proportion (in semitones) given
      #
      # - +prop+: a proportional factor (should be in the range 0-1)
      # - +other+: the other pitch
      #
      def interval_proportion(prop, other)
        self.interval(other) * prop
      end

      #:doc:
      #
      # +add_semitones(semi)+:
      #
      # returns a +PitchClass+ the addition of semitones required by argument
      #
      # - +semi+: the semitones to add; can be any sort of +Numeric+, also
      #           negative to subtract.
      #
      def add_semitones(semi)
        self.class.create_from_semitones((self.inner_representation + semi.to_f).cround)
      end

      #:doc:
      #
      # +add_semitones!(semi)+:
      #
      # returns the addition of semitones required in the receiver's pitch class
      #
      # - +semi+: the semitones to add; can be any sort of +Numeric+, also
      #           negative to subtract.
      #:nodoc:
      def add_semitones!(semi)
        @semi += semi.to_f
        self
      end

      #:doc
      #
      # +anglo_note_name+:
      #
      # returns the name of the note, in anglosaxon notation, with the sharps
      # as only enharmonics (reported as 's'). The octave considers central C
      # as C4
      #
      #
      SEMI_NAMES = %w(C Cs D Ds E F Fs G Gs A As B)
      OCT_DIFF = 4

      def anglo_note_name
        oct = self.octave - OCT_DIFF
        s = self.semi.round
        oct = self.inner_representation < 0.0 && s != 0.0 ? oct-1 : oct
        name = SEMI_NAMES[s]
        "%s%d" % [ name, oct ]
      end

      class << self

        #:doc:
        #
        # +create_from_semitones+
        #
        # creates a +PitchClass+ object out of a semitone representation
        #
        #:nodoc:
        def create_from_semitones(s)
          new(0.0, s)
        end

  
        #:doc:
        #
        # +from_freq+
        #
        # returns a +pitch class+ object from a frequency (Hz)
        #
        #:nodoc:
        def from_freq(f)
          new(f.cpspch)
        end

        #:doc:
        #
        # +find_by_anglo_name(anglo_name = 'C4')+
        #
        # creates a +PitchClass+ object passing a note name in anglosaxon
        # notation (where +C4+ corresponds to +8.00+)
        #
        def find_by_anglo_name(anglo_name = 'C4')
          anglo_name = anglo_name.upcase
          raise ArgumentError, "malformed name #{anglo_name}" unless anglo_name.match?(/\A[A-G]S?\-?\d+\Z/)
          note = anglo_name.sub(/\A([A-G]S?).*\Z/i, '\1')
          upcased_names = SEMI_NAMES.map { |n| n.upcase }
          semi = upcased_names.index(note)
          oct = anglo_name.sub(/\A[A-G]S?(\-?\d+)\Z/, '\1').to_i + OCT_DIFF
#         semi = oct <= 0 ? -semi : semi
          new(oct, semi)
        end

      end
  
    private

      def setup_with_one_semitone_argument(semival)
        setup_with_one_argument(semival.semitopch)
      end
  
      def setup_with_one_argument(fval)
        (o, s) = separate_oct_semi(fval)
        setup_with_two_arguments(o, s)
      end

      def separate_oct_semi(fval)
        octave = fval >= 0.0 ? fval.floor : fval.ceil
        s = (fval-octave) * ::Numeric::PCC
        [octave, s]
      end

      #
      # +setup_with_two_arguments(oct, semi)+:
      #
      # we must make sure that the two values are congruent, that is:
      # +oct+ is a float number and +semi+ is between 0..11.999999
      # taking into account that pitch classes can be positive or negative
      #
      def setup_with_two_arguments(oct, s)
        (oct * ::Numeric::CNPO) + s
      end

    end

  end
end
