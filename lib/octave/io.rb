module Octave

  module Io

    class << self

      OCTAVE_EXE='/usr/bin/env octave -q'
      #
      # +Octave::Io.command(command_string)+
      #
      # send a command to Octave in a pipe and return the results.
      # The results are returned as arrays of float values
      #
      def command(command_string)
        res = []
        decorated_command_string = "echo \"#{command_string}\" | #{OCTAVE_EXE}"
        IO.popen(decorated_command_string) do
          |octave_result|
          out = octave_result.readlines.map { |l| l.chomp }
          out.each { |line| res << line.to_f if line.match?(/\s*\d+/) }
        end
        res
      end

    end

  end

end
