module Octave

  module Io

    class << self

      #
      # +Octave::Io.polyfit(x, y, n)+
      #
      # get the coefficients of an n-degree polynomial
      # using +octave+'s +polyfit+ function
      #
      def polyfit(x, y, n)
        command_string = "a = polyfit(#{x}, #{y}, #{n}); a'"
        ::Octave::Io.command(command_string)
      end

    end

  end

end
