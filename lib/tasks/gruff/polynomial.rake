require 'fileutils'
require File.expand_path(File.join(['..'] * 4, 'spec', 'fixtures', 'polynomial_dataset'), __FILE__)
require File.expand_path(File.join('..', 'gruff_plot'), __FILE__)

namespace :plot do

  desc 'plot the Polynomial dataset'
  task :polynomial => :create_output_dir do
    ranges = [ Range.new(0, 0) ]
    p = Mext::Gruff::Plotter.new('polynomial_dataset', 800, Mext::Spec::Fixtures::Math::POLYNOMIAL_DATASET, ranges)
    p.plot do
      |args|
      Math::Polynomial.new(args)
    end
  end

end
