module Mext

  PATH = File.expand_path(File.join('..', 'mext'), __FILE__)

end

%w(
  exceptions
  utilities
  random_generator
  numeric
  array
  math
  music
  sound
).each { |f| require File.join(Mext::PATH, f) }
